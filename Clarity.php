<!DOCTYPE html>
<html>
<body>
    <head>
    <style>table, th, td {
      border: 1px solid black;
    }</style>
    </head>


<h1>Question 1</h1>
<?php

$list = ["John", "Mary", "Sam", "Jane"];


foreach($list as $name){
    echo $name . "<br>";
}
?>


<h1>Question 2 </h1>

<?php


$text = "Enthusiasm";


function count_Vowels($text)
{
    preg_match_all('/[aeiou]/i', $text, $matches);
    return count($matches[0]);
}
$count = count_Vowels($text);

echo "Total Vowels : $count";


?>

<h1>Question 3 </h1>

<?php
$n = 10;
for ($i = $n; $i > 4; $i--) {
    if (($i % 2) != 0) {

        echo $i . "<br>";
    }
}
?>

<h1>Question 4 </h1>



<?php

// This is an array that contains all the items the Shop currently have
$inventory = [
    '10001' => [
        'name' => 'Google Pixel 3a XL', // Name of the product
        'price' => 200, // Price of the product
        'balance' => 3, //	Stock balance of the product
    ],
    '10002' => [
        'name' => 'iPhone 14 Pro Max',
        'price' => 900,
        'balance' => 1,
    ],
    '10003' => [
        'name' => 'Samsung Galaxy S50',
        'price' => 450,
        'balance' => 4,
    ],
    '10004' => [
        'name' => 'BlackBerry Q10',
        'price' => 180,
        'balance' => 2,
    ],
  ];
  // The items added to cart
  $cart = [
    'items' => [
        '10001' => [
            'name' => 'Google Pixel 3a XL', // Name of the product
            'price' => 200, // Price of the product
            'quantity' => 2, // Quantity of product
        ],
        '10002' => [
            'name' => 'iPhone 14 Pro Max',
            'price' => 900,
            'quantity' => 1,
        ],
    ],
    'total_price' => 1300, // Total price of all the items in the cart array
];

{/* */}
function add_to_cart($item_id, $quantity) {
    global $inventory, $cart; {/* add global to both array cause it is out of the function*/}

    if (!isset($inventory[$item_id]) || $quantity <= 0) {   {/* isset determines the id and the quantity is not smaller than 0*/}
        echo "Invalid item or quantity. Please try again."; 
        return;
    }
    if (isset($cart['items'][$item_id])) {
        $cart['items'][$item_id]['quantity'] += $quantity; {/* update the quantity if the item already inside a cart */}
    } else {
        $cart['items'][$item_id] = [               // add the new item,price and quantity if the item is not inside the cart already
            'name' => $inventory[$item_id]['name'],
            'price' => $inventory[$item_id]['price'],
            'quantity' => $quantity
        ];
    }
    $inventory[$item_id]['balance'] -= $quantity; {/* update the balance from the inventory */}
    $cart['total_price'] += $quantity * $inventory[$item_id]['price']; //total price of every item
    echo "<table>";
    echo "<tr>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
        </tr>";
    foreach ($cart['items'] as $item) {
        $each_total = $item['price'] * $item['quantity']; //total price of each item
        echo "<tr>
                <td>" . $item['name'] . "</td>
                <td>" . $item['price'] . "</td>
                <td>" . $item['quantity'] . "</td>
                <td>" . $each_total . "</td>
            </tr>";
    }

    echo "<tr>
            <td colspan='3'>Total Price: </td>
            <td>" . $cart['total_price']. "</td>
        </tr>";
    echo "</table>";
}

add_to_cart('10004', 3); //things to add to cart to optional






?>


</body>
</html>
